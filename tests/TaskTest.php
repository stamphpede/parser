<?php

namespace Stamphpede\Parser;

use PHPUnit\Framework\TestCase;
use RingCentral\Psr7\Request as Psr7Request;
use Stamphpede\Annotation;
use Stamphpede\Request;
use Stamphpede\ResponseCollection;

/**
 * @covers \Stamphpede\Parser\Task
 * @covers \Stamphpede\Parser\ParserException
 * @uses \Stamphpede\ResponseCollection
 * @uses \Stamphpede\Request
 * @uses \Stamphpede\Annotation\Depends
 * @uses \Stamphpede\Annotation\Cacheable
 * @uses \Stamphpede\Annotation\Repeatable
 * @uses \Stamphpede\Annotation\Task
 * @uses \Stamphpede\Annotation\Weight
 */
class TaskTest extends TestCase
{
    public function testCreateFromAnnotations()
    {
        $callback = function () {};
        $annotations = [
            new Annotation\Task(['value' => 'test']),
            new Annotation\Weight(['value' => 4]),
            new Annotation\Repeatable(['value' => Annotation\Repeatable::REPEAT_END]),
            new Annotation\Depends(['value' => 'subtask']),
            new Annotation\Depends(['value' => 'subtask2']),
        ];

        $sut = Task::fromAnnotations($callback, $annotations);

        self::assertEquals('test', $sut->getName());
        self::assertEquals(4, $sut->getWeight());
        self::assertTrue($sut->endRepeat());
        self::assertFalse($sut->skipRepeat());
        self::assertFalse($sut->ignoreRepeat());
        self::assertEquals(['subtask', 'subtask2'], $sut->getDepends());
    }

    public function testGetRequestCached()
    {
        $expected = new Request('test', new Psr7Request('GET', 'http://localhost'));

        $callback = function () {
            return new Request('test', new Psr7Request('GET', 'http://localhost'));
        };

        $annotations = [
            new Annotation\Task(['value' => 'test']),
        ];

        $sut = Task::fromAnnotations($callback, $annotations);
        $responseCollection = new ResponseCollection();
        $request = $sut->getRequest($responseCollection);

        self::assertEquals($expected, $request);
        self::assertSame($request, $sut->getRequest($responseCollection));
    }

    public function testGetRequestNoCache()
    {
        $callback = function () {
            return new Request('test', new Psr7Request('GET', 'http://localhost'));
        };

        $annotations = [
            new Annotation\Task(['value' => 'test']),
            new Annotation\Cacheable(['value' => false]),
        ];

        $sut = Task::fromAnnotations($callback, $annotations);
        $responseCollection = new ResponseCollection();
        $request = $sut->getRequest($responseCollection);

        self::assertNotSame($request, $sut->getRequest($responseCollection));
    }

    public function testInvalidAnnotationValue()
    {
        $callback = function () {};
        $annotations = [
            new Annotation\Task(['value' => 'test']),
            new Annotation\Weight(['value' => 4]),
            new Annotation\Repeatable(['value' => 'foo']),
            new Annotation\Depends(['value' => 'subtask']),
            new Annotation\Depends(['value' => 'subtask2']),
        ];

        $this->expectException(ParserException::class);
        $this->expectExceptionMessage(
            ParserException::invalidAnnotationValue(Annotation\Repeatable::class, 'foo')->getMessage()
        );

        Task::fromAnnotations($callback, $annotations);
    }
}
