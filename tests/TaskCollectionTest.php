<?php

namespace Stamphpede\Parser;

use PHPUnit\Framework\TestCase;
use RingCentral\Psr7\Request as Psr7Request;
use Stamphpede\Annotation;
use Stamphpede\Request;
use Stamphpede\ResponseCollection;

/**
 * @covers \Stamphpede\Parser\TaskCollection
 * @covers \Stamphpede\Parser\ParserException
 * @uses \Stamphpede\Parser\Task
 * @uses \Stamphpede\Annotation\Task
 */
class TaskCollectionTest extends TestCase
{
    public function testIterate()
    {
        $tasks = [
            Task::fromAnnotations(function () {}, [new Annotation\Task(['value' => 'task1'])]),
            Task::fromAnnotations(function () {}, [new Annotation\Task(['value' => 'task2'])]),
        ];

        $sut = new TaskCollection(...$tasks);
        $result = [];

        foreach ($sut as $key => $value) {
            $result[$key] = $value;
        }

        self::assertEquals(['task1' => $tasks[0], 'task2' => $tasks[1]], $result);
    }

    public function testGet()
    {
        $tasks = [
            Task::fromAnnotations(function () {}, [new Annotation\Task(['value' => 'task1'])]),
            Task::fromAnnotations(function () {}, [new Annotation\Task(['value' => 'task2'])]),
        ];

        $sut = new TaskCollection(...$tasks);

        self::assertEquals($tasks[0], $sut->get('task1'));
    }

    public function testDuplicateTaskError()
    {
        $tasks = [
            Task::fromAnnotations(function () {}, [new Annotation\Task(['value' => 'task1'])]),
            Task::fromAnnotations(function () {}, [new Annotation\Task(['value' => 'task1'])]),
        ];

        $this->expectException(ParserException::class);
        $this->expectExceptionMessage('You have two tasks with the same name (task1)');

        new TaskCollection(...$tasks);
    }
}
