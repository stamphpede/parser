<?php

namespace Stamphpede\Parser;

use PHPUnit\Framework\TestCase;
use Stamphpede\Annotation\Concurrency;
use Stamphpede\Annotation\Duration;
use Stamphpede\Annotation\RampUp;

/**
 * @covers \Stamphpede\Parser\Stamphpede
 * @uses \Stamphpede\Annotation\Concurrency
 * @uses \Stamphpede\Annotation\RampUp
 * @uses \Stamphpede\Annotation\Duration
 * @uses \Stamphpede\Parser\TaskCollection
 */
class StamphpedeTest extends TestCase
{
    public function testCreateFromAnnotations()
    {
        $annotations = [
            new Concurrency(['value' => 7]),
            new RampUp(['value' => 30]),
            new Duration(['value' => 120])
        ];

        $sut = Stamphpede::fromAnnotations($annotations);

        self::assertEquals(7, $sut->getConcurrency());
        self::assertEquals(30, $sut->getRampUp());
        self::assertEquals(120, $sut->getDuration());
    }

    public function testGetTasks()
    {
        $tasks = new TaskCollection();

        $sut = Stamphpede::fromAnnotations([]);
        $sut = $sut->withTasks($tasks);

        $result = $sut->getTasks();

        self::assertEquals($tasks, $result);
    }

    /** @dataProvider provideGetStartingConcurrency */
    public function testGetStartingConcurrency(int $rampup, int $concurrency, int $expected)
    {
        $annotations = [
            new Concurrency(['value' => $concurrency]),
            new RampUp(['value' => $rampup])
        ];

        $sut = Stamphpede::fromAnnotations($annotations);

        self::assertEquals($expected, $sut->getStartingConcurrency());
    }

    public function provideGetStartingConcurrency()
    {
        return [
            [0, 10, 10],
            [5, 10, 2],
            [3, 10, 4],
            [25, 10, 1],
        ];
    }

    /** @dataProvider provideGetRampupStep */
    public function testGetRampupStep(int $rampup, int $concurrency, int $expected)
    {
        $annotations = [
            new Concurrency(['value' => $concurrency]),
            new RampUp(['value' => $rampup])
        ];

        $sut = Stamphpede::fromAnnotations($annotations);

        self::assertEquals($expected, $sut->getRampUpStep());
    }

    public function provideGetRampupStep()
    {
        return [
            [0, 10, 0],
            [5, 10, 2],
            [3, 10, 3],
            [25, 10, 1],
        ];
    }

    /** @dataProvider provideGetRampupStepTime */
    public function testGetRampupStepTime(int $rampup, int $concurrency, float $expected)
    {
        $annotations = [
            new Concurrency(['value' => $concurrency]),
            new RampUp(['value' => $rampup])
        ];

        $sut = Stamphpede::fromAnnotations($annotations);

        self::assertEquals($expected, $sut->getRampUpStepTime());
    }

    public function provideGetRampupStepTime()
    {
        return [
            [0, 10, 1],
            [5, 10, 1],
            [3, 10, 1],
            [25, 10, 2.5],
        ];
    }
}
