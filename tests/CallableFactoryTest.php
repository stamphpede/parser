<?php

namespace Stamphpede\Parser;

use PHPUnit\Framework\TestCase;
use RingCentral\Psr7\Response as Psr7Response;
use Stamphpede\Response;
use Stamphpede\ResponseCollection;

/**
 * @covers \Stamphpede\Parser\CallableFactory
 * @covers \Stamphpede\Parser\ParserException
 * @uses \Stamphpede\ResponseCollection
 * @uses \Stamphpede\Response
 */
class CallableFactoryTest extends TestCase
{
    public function testObjectMethodWithNoParameters()
    {
        $testCase = new class () {
            public bool $called = false;
            public function testMethod() {
                $this->called = true;
            }
        };

        $responseCollection = new ResponseCollection();

        $sut = new CallableFactory();
        $callable = $sut->create($testCase, 'testMethod');
        $callable($responseCollection);

        self::assertTrue($testCase->called, 'Callable didn\'t call the expected method');
    }

    public function testObjectMethodWithResponseCollection()
    {
        $testCase = new class () {
            public bool $called = false;
            public function testMethod(ResponseCollection $collection) {
                $this->called = true;
            }
        };

        $responseCollection = new ResponseCollection();

        $sut = new CallableFactory();
        $callable = $sut->create($testCase, 'testMethod');
        $callable($responseCollection);

        self::assertTrue($testCase->called, 'Callable didn\'t call the expected method');
    }

    public function testObjectMethodWithResponse()
    {
        $testCase = new class () {
            public bool $called = false;
            public function testMethod(Response $collection) {
                $this->called = true;
            }
        };

        $response = new Response('test', new Psr7Response(200));
        $responseCollection = ResponseCollection::fromResponses($response);

        $sut = new CallableFactory();
        $callable = $sut->create($testCase, 'testMethod');
        $callable($responseCollection);

        self::assertTrue($testCase->called, 'Callable didn\'t call the expected method');
    }

    /** @dataProvider provideErrorConditions */
    public function testErrorConditions(object $class, string $method, string $message)
    {
        $sut = new CallableFactory();

        $this->expectException(ParserException::class);
        $this->expectExceptionMessage($message);

        $sut->create($class, $method);
    }

    public function provideErrorConditions()
    {
        $testcase = new class () {
            public function noTypeHint($parameter)
            {
            }

            public function tooManyParams(ResponseCollection $responseCollection, $secondParam)
            {
            }

            public function badTypeHint(\stdClass $parameter)
            {
            }
        };

        return [
            [
                $testcase,
                'noTypeHint',
                'Method noTypeHint has no type hint, this factory is only capable of using methods with parameter type of Response or ResponseCollection'
            ],
            [
                $testcase,
                'tooManyParams',
                'Method tooManyParams has 2 parameters, this factory is only capable of using methods with 1 or 0 parameters'
            ],
            [
                $testcase,
                'badTypeHint',
                'Method badTypeHint parameter type is stdClass, this factory is only capable of using methods with parameter type of Response or ResponseCollection'
            ],
        ];
    }
}
