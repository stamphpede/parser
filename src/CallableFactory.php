<?php

namespace Stamphpede\Parser;

use Stamphpede\Response;
use Stamphpede\ResponseCollection;

class CallableFactory
{
    public function create(object $class, string $method): callable
    {
        $reflect = new \ReflectionClass($class);
        $reflectMethod = $reflect->getMethod($method);
        $parameters = $reflectMethod->getParameters();
        if (empty($parameters)) {
            return function (ResponseCollection $responses) use ($class, $method) {
                return $class->$method();
            };
        }

        if (count($parameters) > 1) {
            throw ParserException::invalidParameterCount($method, count($parameters));
        }

        $parameterType = $parameters[0]->getType();

        if (!($parameterType instanceof \ReflectionNamedType)) {
            throw ParserException::noParameterType($method);
        }

        $typeName = $parameterType->getName();
        switch($typeName) {
            case Response::class:
                return function (ResponseCollection $responses) use ($class, $method) {
                    return $class->$method($responses->getIterator()->current());
                };
            case ResponseCollection::class:
                return function (ResponseCollection $responses) use ($class, $method) {
                    return $class->$method($responses);
                };
        }

        throw ParserException::invalidParameterType($method, $typeName);
    }
}
