<?php

namespace Stamphpede\Parser;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

class AnnotationReaderFactory
{
    public static function create()
    {
        AnnotationRegistry::registerLoader('class_exists');
        return new AnnotationReader();
    }
}
