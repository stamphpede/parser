<?php

namespace Stamphpede\Parser;

use Doctrine\Common\Annotations\AnnotationReader;

class Module
{
    public function getServices(): array
    {
        return [
            AnnotationReader::class => [
                'factory' => [AnnotationReaderFactory::class, 'create'],
            ],
            StamphpedeFactory::class => [
                'args' => [
                    AnnotationReader::class,
                    'config',
                ],
            ],
        ];
    }
}
