<?php

namespace Stamphpede\Parser;

use Stamphpede\Annotation;

class Stamphpede
{
    private int $duration = 30;
    private int $concurrency = 2;
    private int $rampUp = 0;
    private ?TaskCollection $tasks = null;

    private function __construct()
    {
    }

    public static function fromAnnotations(array $annotations): self
    {
        $instance = new self();
        foreach ($annotations as $annotation) {
            switch (get_class($annotation)) {
                case Annotation\Duration::class:
                    $instance->duration = $annotation->getMax();
                    break;
                case Annotation\RampUp::class:
                    $instance->rampUp = $annotation->getDuration();
                    break;
                case Annotation\Concurrency::class:
                    $instance->concurrency = $annotation->getMax();
                    break;
            }
        }

        return $instance;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getConcurrency(): int
    {
        return $this->concurrency;
    }

    public function getRampUp(): int
    {
        return $this->rampUp;
    }

    public function getStartingConcurrency(): int
    {
        if ($this->getRampUp() === 0) {
            return $this->concurrency;
        }

        $rampupStep = $this->getRampUpStep();
        $extra = max(0, $this->concurrency - ($rampupStep * $this->rampUp));

        return $rampupStep + $extra;
    }

    public function getRampUpStep(): int
    {
        if ($this->getRampUp() === 0) {
            return 0;
        }

        return max(1, intval($this->concurrency / $this->rampUp));
    }

    public function getRampUpStepTime(): float
    {
        if ($this->getRampUp() === 0) {
            return 1;
        }

        if (($this->concurrency / $this->rampUp) > 1) {
            return 1;
        }

        return $this->rampUp / $this->concurrency;
    }

    public function getTasks(): ?TaskCollection
    {
        return $this->tasks;
    }

    public function withTasks(TaskCollection $tasks): self
    {
        $instance = clone $this;
        $instance->tasks = $tasks;

        return $instance;
    }
}
