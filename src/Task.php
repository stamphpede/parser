<?php

namespace Stamphpede\Parser;

use Stamphpede\Annotation;
use Stamphpede\Request;
use Stamphpede\ResponseCollection;

class Task
{
    private int $weight = 1;
    private string $repeatable = Annotation\Repeatable::REPEAT_OK;
    private string $name = '';
    private array $depends = [];
    /** @var callable */
    private $callable;
    private ?Request $request = null;
    private bool $cacheable = true;

    private function __construct()
    {
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDepends(): array
    {
        return $this->depends;
    }

    public function getRequest(ResponseCollection $responseCollection): Request
    {
        if ($this->request === null || !$this->cacheable) {
            $callable = $this->callable;
            $request = $callable($responseCollection);

            if ($request instanceof Request) {
                $request = $request->getPsrRequest();
            }

            $this->request = new Request($this->name, $request);
        }

        return $this->request;
    }

    public static function fromAnnotations(callable $callable, array $annotations): Task
    {
        $instance = new self();
        $instance->callable = $callable;
        foreach ($annotations as $annotation) {
            switch (get_class($annotation)) {
                case Annotation\Task::class:
                    $instance->name = $annotation->getName();
                    break;
                case Annotation\Repeatable::class:
                    $behaviour = $annotation->getBehaviour();
                    if (!in_array($behaviour, Annotation\Repeatable::REPEAT)) {
                        throw ParserException::invalidAnnotationValue('repeatable', (string) $behaviour);
                    }
                    $instance->repeatable = $behaviour;
                    break;
                case Annotation\Weight::class:
                    $instance->weight = $annotation->getWeight();
                    break;
                case Annotation\Depends::class:
                    $instance->depends[] = $annotation->getOn();
                    break;
                case Annotation\Cacheable::class:
                    $instance->cacheable = $annotation->isCacheable();
                    break;
            }
        }

        return $instance;
    }

    public function ignoreRepeat(): bool
    {
        return $this->repeatable === Annotation\Repeatable::REPEAT_IGNORE;
    }

    public function skipRepeat(): bool
    {
        return $this->repeatable === Annotation\Repeatable::REPEAT_SKIP;
    }

    public function endRepeat(): bool
    {
        return $this->repeatable === Annotation\Repeatable::REPEAT_END;
    }
}
