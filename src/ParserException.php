<?php

namespace Stamphpede\Parser;

class ParserException extends \RuntimeException
{
    public static function invalidParameterCount(string $method, int $count): self
    {
        return new self(
            sprintf('Method %s has %d parameters, this factory is only capable of using methods with 1 or 0 parameters',
                $method,
                $count
            )
        );
    }

    public static function invalidParameterType(string $method, string $type): self
    {
        return new self(
            sprintf('Method %s parameter type is %s, this factory is only capable of using methods with parameter type of Response or ResponseCollection',
                $method,
                $type
            )
        );
    }

    public static function noParameterType(string $method): self
    {
        return new self(
            sprintf('Method %s has no type hint, this factory is only capable of using methods with parameter type of Response or ResponseCollection',
                $method
            )
        );
    }

    public static function invalidAnnotationValue(string $annotation, string $value): self
    {
        return new self(
            sprintf('Invalid value (%s) provided for annotation %s', $value, $annotation)
        );
    }

    public static function duplicatedTask(string $taskName): self
    {
        return new self(sprintf('You have two tasks with the same name (%s)', $taskName));
    }
}
