<?php

namespace Stamphpede\Parser;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class StamphpedeFactory
{
    private AnnotationReader $reader;
    private CallableFactory $callableFactory;
    private ParameterBag $parameters;

    public function __construct(AnnotationReader $reader, ParameterBag $config)
    {
        $this->reader = $reader;
        $this->callableFactory = new CallableFactory();
        $parameters = [];

        if ($config->has('parameters')) {
            $parameters = $config->get('parameters');
        }

        $this->parameters = new ParameterBag($parameters);
    }

    public function loadFile(string $file): Stamphpede
    {
        $testCase = require $file;
        $reflectionClass = new \ReflectionClass($testCase);

        $annotations = $this->getAnnotations($reflectionClass);
        $stamphpede = Stamphpede::fromAnnotations($annotations);
        $methods = $this->getValidTaskMethods($reflectionClass);

        $tasks = [];

        foreach ($methods as $method) {
            try {
                $annotations = $this->getMethodAnnotations($method);
                $callable = $this->callableFactory->create($testCase, $method->getName());
                if ($this->isTask($annotations)) {
                    $task = Task::fromAnnotations($callable, $annotations);
                    $tasks[] = $task;
                }
            } catch (\Throwable $t)
            { echo $t->getMessage();}
        }

        $stamphpede = $stamphpede->withTasks(new TaskCollection(... $tasks));

        if($this->canInjectParameters($testCase)) {
            $testCase->injectParameters($this->parameters);
        }

        return $stamphpede;
    }

    private function canInjectParameters(object $testCase): bool
    {
        return method_exists($testCase, 'injectParameters');
    }

    private function getValidTaskMethods(\ReflectionClass $reflectionClass): array
    {
        $methods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);
        return array_filter($methods, function ($method) {
            // If the method is abstract or static then return false (remove it from the list)
            return !($method->isAbstract() || $method->isStatic() || substr($method->getName(), 0, 6) === 'inject');
        });
    }

    private function getAnnotations(\ReflectionClass $reflectionClass): array
    {
        return $this->reader->getClassAnnotations($reflectionClass);
    }

    private function getMethodAnnotations(\ReflectionMethod $method): array
    {
        return $this->reader->getMethodAnnotations($method);
    }

    private function isTask(array $annotations): bool
    {
        foreach ($annotations as $annotation) {
            if (get_class($annotation) === \Stamphpede\Annotation\Task::class) {
                return true;
            }
        }

        return false;
    }
}
