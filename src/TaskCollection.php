<?php

namespace Stamphpede\Parser;

class TaskCollection implements \Iterator
{
    private array $data = [];
    public function __construct(Task ...$tasks)
    {
        foreach ($tasks as $task) {
            if (isset($this->data[$task->getName()])) {
                throw ParserException::duplicatedTask($task->getName());
            }
            $this->data[$task->getName()] = $task;
        }
    }

    public function current(): ?Task
    {
        return \current($this->data) ?: null;
    }

    public function next(): ?Task
    {
        return \next($this->data) ?: null;
    }

    public function key(): ?string
    {
        return \key($this->data) ?: null;
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind(): void
    {
        \reset($this->data);
    }

    public function get(string $taskName): Task
    {
        return $this->data[$taskName];
    }
}
